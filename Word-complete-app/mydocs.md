### How to use the application in order to predict the next word in a sentence

To do this, open Predict tab and type your excerpt into the text box. Once you have opened the application, it needs to wait for 10 sec. After that pause, you will obtain results 'online' without further pauses.  

### The gist of the algorithm

This algorithm is based on simple backoff model. The longest ngrams used are four grams. It's accuracy (percentage of sentences completed correctly) is about 19%. Before creating ngrams, the following transformation with data  had been performed:

* Upper case to lower case transformation.
* Removal of punctuation, twitter signs, hyphens and quotation marks.
* Removal of standalone numbers (although, words with numbers included remained untouched).
* Long word removal (for example, iwanttogooutside).

Ngrams, observed just once were removed, because there are a lot of misspellings, words came from foreign languages and rare non-word symbols in the text corpora. It helps to diminish the size of a language model (about 20 times) and improve it's performance (by about 2%, from 16% to 18%). 

Notice, that I haven't deleted any stopwords and profane words.
\n