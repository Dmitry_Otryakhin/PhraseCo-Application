English phrase completion with a backoff model.
========================================================
author: Otryakhin D.
date: Sat Apr 16 12:50:48 2016
autosize: true
transition: rotate

Introduction
========================================================

This app is designed to show how a [backoff model] (https://en.wikipedia.org/wiki/Katz's_back-off_model) can manage phrase completion task. Working language is English, but the algorithm could be trained on any other European language.

The data used for model training and testing came from
<https://corpora.heliohost.org>.
It contains text strings from blogs, Twitter and various news reports.

A complete user-friendly application could be found [here](https://dmitryotryakhin.shinyapps.io/Word-complete-app/).

How it works
========================================================

Open Predict tab and type your excerpt into the text box in order to obtain predictions. The app will instantly suggest the best option to choose and a list of all possible options.

After you've typed a phrase, the algorithm clean and tokenize it, and then go over ngram storage to find the best word (from 4-grams to 2-grams).

If you need some explanations, open Help tab.

About the model
========================================================

As I mentioned, this software uses a backoff language model. It consists of 4-, 3-, and 2- grams. Before ngram creation, the following transformations had been applied to the data:

- Upper case to lower case transformation.
- Removal of punctuation, twitter signs, hyphens and quotation marks.
- Removal of standalone numbers.
- Long word removal.

Ngrams, observed just once were considered to be unreliable and removed.

Features
========================================================

Feature  | Measure
---------|-------------
Max length of an ngram| 4
Size of the ngram storage | 11 MB
Accuracy | 19%


R language is used

libraries: foreach, tm, reshape2, caret, rpart, quanteda, plyr, shiny

Ngram storage developed to be highly scalable (partly owing to quanteda package). Although, this app operates well even on a very small amount of data.

